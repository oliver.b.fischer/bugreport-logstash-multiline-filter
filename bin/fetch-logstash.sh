#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

. ${DIR}/prop.conf

curl -O https://download.elasticsearch.org/logstash/logstash/${LS_DIST}

LS_BASE_NAME=$( basename ${LS_DIST} .tar.gz)

mkdir ${DIR}/../wrkdir

mv ${LS_DIST} ${DIR}/../wrkdir

tar -C ${DIR}/../wrkdir/ -z -x -v -f ${DIR}/../wrkdir/${LS_DIST}

mv ${DIR}/../wrkdir/${LS_BASE_NAME} ${DIR}/../wrkdir/logstash
