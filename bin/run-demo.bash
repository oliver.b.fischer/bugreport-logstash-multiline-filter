#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

. ${DIR}/prop.conf


mkdir -p ${DIR}/../wrkdir

function_write_logfile() {

  sleep 20

  rm -v -f ${DIR}/../wrkdir/input.log

  cat /dev/null > ${DIR}/../wrkdir/input.log

  echo -n "0000000" >> ${DIR}/../wrkdir/input.log

  for (( i=1; i <= 50; i++ ))
  do
      echo -n -e "\nThis is line $i" >> ${DIR}/../wrkdir/input.log

  done

  echo -n -e "\nI am the last line" >> ${DIR}/../wrkdir/input.log
}

# write config 

cat<<EOF>${DIR}/../wrkdir/input.conf
input {
  file {
    path => "${DIR}/../wrkdir/input.log"
  }
}

output {
  stdout {
  }
}
EOF




function_write_logfile &

bash ${DIR}/../wrkdir/logstash/bin/logstash agent -v -f ${DIR}/../wrkdir/input.conf





