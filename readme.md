# Showcase for problem of the lost last line of Logstash's multiline filter

This show case demonstrates that Logstash's multiline filter (and multiline codec) 
are unable to read the last written line of a logfile.

This problem has been reported to the Logstash team and is addressed 
by the following issues:

- http://logstash.jira.com/browse/LOGSTASH-1697
- http://logstash.jira.com/browse/LOGSTASH-271

## How to run this show case

Requirements:

    git clone https://bitbucket.org/obfischer/bugreport-logstash-multiline-filter.git
    cd bugreport-logstash-multiline-filter
    rm -r -f wrkdir
    bash bin/fetch-logstash.sh
    bash bin/run-demo.bash



   